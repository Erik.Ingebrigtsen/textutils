package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.impl.AlignerA;
import no.uib.ii.inf112.impl.AlignerB;
import no.uib.ii.inf112.impl.AlignerC;
import no.uib.ii.inf112.impl.AlignerD;
import no.uib.ii.inf112.impl.AlignerE;
import no.uib.ii.inf112.impl.AlignerF;
import no.uib.ii.inf112.impl.AlignerG;
import no.uib.ii.inf112.impl.AlignerH;

public abstract class TestAligner {
	TextAligner aligner;

	@Test
	void testCenter() {
		assertEquals("  ABC  ", aligner.center("ABC", 7));
		assertEquals("  ABC ", aligner.center("ABC", 6));
		assertEquals("A", aligner.center("A", 1));
		assertEquals(" ABCD", aligner.center("ABCD", 5));
		//Test for longer word than line length
		assertEquals("A", aligner.center("A", 0));
		
		
	}

	@Test
	void testFlushRight() {
		assertEquals("    ABC", aligner.flushRight("ABC", 7));
		assertEquals("  ABC", aligner.flushRight(" ABC", 5));
		assertEquals("  ABC", aligner.flushRight(" ABC ", 5));
		assertEquals("  ABC", aligner.flushRight("ABC ", 5));
		//Test for longer word than line length
		assertEquals("A", aligner.flushRight("A", 0));

	}

	@Test
	void testFlushLeft() {
		assertEquals("ABC    ", aligner.flushLeft("ABC", 7));
		assertEquals("ABC  ", aligner.flushLeft("ABC ", 5));
		assertEquals("ABC  ", aligner.flushLeft(" ABC ", 5));
		assertEquals("ABC  ", aligner.flushLeft(" ABC", 5));
		//Test for longer word than line length
		assertEquals("A", aligner.flushLeft("A", 0));

	}

	@Test
	void testJustify() {
		assertEquals("Justify   this   text", aligner.justify("Justify this text", 21));
		assertEquals("Justify   ", aligner.justify("Justify", 10));
		//Test for longer word than line length
		assertEquals("A", aligner.justify("A", 0));
		
	}
}

class TestAlignerA extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerA();
	}
}

class TestAlignerB extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerB();
	}
}

class TestAlignerC extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerC();
	}
}

class TestAlignerD extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerD();
	}
}

class TestAlignerE extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerE();
	}
}

class TestAlignerF extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerF();
	}
}

class TestAlignerG extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerG();
	}
}

class TestAlignerH extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerH();
	}
}